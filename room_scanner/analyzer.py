import cv2
from photo_set import *

class Analyzer:

    __instance = None # this class is designed as a Singleton
    pts = []

    def getInstance():
        """ static access method """
        if Analyzer.__instance is None:
            Analyzer()

        return Analyzer.__instance

    def __init__(self):
        """ virtually private constructor """
        if Analyzer.__instance is None:
            Analyzer.__instance = self


    def grab_point(self,event, x, y, flags, param):
        """ grab_point is the function given to openCV to handle
            any events that take place on the image when it is displayed.

            Specifically, if the left mouse button was clicked, the (x, y)
            are pushed back into the Analyzers pts list """

        if event == cv2.EVENT_LBUTTONDOWN:
            self.pts.append((x, y))

    def calc_dist(self, pt1, pt2):
        """ calculates the linear distance between two
            points returns this distance (in pixels) """

        return ((pt1[0] -pt2[0])**2 + (pt1[1] - pt2[1])**2)**(1/2)

    #Use this function to get dimensions, returns in a pair
    def get_dimensions(self, pano):

        wall_1 = self.get_wall(pano) # grab first wall
        dim1 = self.calc_dist(wall_1[0], wall_1[1])

        wall_2 = self.get_wall(pano) # grab second wall
        dim2 = self.calc_dist(wall_2[0], wall_2[1])

        return dim1, dim2

    def get_wall(self, pano):

        """ get_wall displays an image and waits for click events """

        image = pano.get_pano()
        image_name = pano.get_name()

        cv2.namedWindow(image_name) # name the image, with the panoramas name
        cv2.setMouseCallback(image_name, self.grab_point)
        self.pts = []

        # keep looping until two points have been collected (i.e until pts size is 2)
        while len(self.pts) != 2:

            # display the image and wait for a mouseclick
            cv2.imshow(image_name, image)
            key = cv2.waitKey(1) & 0xFF

        """
        # if the 'r' key is pressed, reset the cropping region
        if key == ord("r"):
        image = clone.copy()

        # if the 'c' key is pressed, break from the loop
        elif key == ord("c"):
        break
        """

        # close all open windows
        cv2.destroyAllWindows()

        return self.pts
