"""Helper objects for the package"""

#pylint: disable=too-few-public-methods
class PhotoSet():
    """A PhotoSet is a named list of photo data.
    It can be stored in a folder on disk.
    """
    def __init__(self, name, photos):
        self.name = name
        self.photo_list = photos

    """ return a photo_set objects name """
    def get_name(self):
        return self.name

    """ return an image, particularly, photo_list[idx] """
    def get_image(self, i):
        return self.photo_list[i]


    """ return list of images stored in this PhotoSet """
    def get_images(self):
        return self.photo_list

    """ return size of list of images stored in this PhotoSet"""
    def get_size(self):
        return len(self.photo_list)


class Panorama():
    """A Panorama is a named image, stitched together
    from the photos in the PhotoSet of the same name.

    It contains key points, used for the Image Analyser
    """
    def __init__(self, name, pano):
        self.name = name
        self.panorama = pano

        #self.pano_file = #TODO

    """ return panorama objects name """
    def get_name(self):
        return self.name


    """ return the stitched image stored in Panorama object """
    def get_pano(self):
        return self.panorama

    #def get_pano_file(self):
        #return self.pano_file


#Inventory set class: hold name and list of inventory items
class Inventory():

    def __init__(self, name):
        self.name = name
        self.items = []

    def add_item(self, item):
        self.items.append(item)
        return item

    #Simple getter functions
    def get_name(self):
        return self.name
    def get_items(self):
        return self.items
    def get_inventory_size(self):
        return len(self.items)


class Item():
    def __init__(self, name, width, length):
        self.name = name
        self.width = width
        self.length = length

    def rotate(self):
        temp = self.width
        self.width = self.length
        self.length = temp

    #Simple getter functions
    def get_name(self):
        return self.name
    def get_width(self):
        return self.width
    def get_length(self):
        return self.length
