"""
Stitcher handles making a panorama out of the list of images.
It also does error checking on the user's input of images.
"""

import cv2

class Stitcher(object): #pylint: disable=too-few-public-methods

    __instance = None # this class is designed as a Singleton

    def getInstance():

        """ static access method """
        if Stitcher.__instance is None:
            Stitcher()

        return Stitcher.__instance

    def __init__(self):
        """ virtually private constructor """
        self.stitcher = cv2.createStitcher(False)
        if Stitcher.__instance is None:
            Stitcher.__instance = self


    def make_panorama(self, photo_set):
        """ Puts the images in the stitches image list
        together into a panorama """


        # get images from photo_set object
        images = photo_set.get_images()


        # stitch all the images together
        pano = self.stitcher.stitch(images)

		# pano is a tuple
		#
		# if pano[0] == 0 --> OK
		# if pano[0] == 1 --> ERR_NEED_MORE_IMGS
		# if pano[0] == 2 --> ERR_HOMOGROPHY_EST_FAIL
		# if pano[0] == 3 --> ERR_CAMERA_PARAMS_ADJUST_FAIL
		#
		# pano[1], if operation successful is the panorama image
        return pano
