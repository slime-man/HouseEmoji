"""
Run this script to run the entire application.

Command line options:
    -debug for debug output
    -nogui for a shell application
"""

# RoomScanner should be the only class that deals
#   with the filesystem.
import os
import sys
import cv2

# Other application modules
import stitcher #from _ import _ to turn module into class
from stitcher import Stitcher
from photo_set import *
from user_interface import user_interface
from analyzer import Analyzer
# import plotter

class RoomScanner:
    """
    RoomScanner is an application that creates a diagram of a room from photos.
        The photos are taken in a panorama style,
        rotating about a point in the room.
    """

    def __init__(self, debug=False, gui=True):
        self.__log("Initialization.")
        self.debug = debug
        self.gui = gui

        self.photo_sets = []
        self.panoramas = []

        self.inventories = []

        #self.user_interface = gui
        if self.gui:
            print("Initializing GUI")
            self.user_interface = user_interface(room_scanner_parent = self,
                debug = self.debug)

        # self.stitcher = Stitcher()
        # self.__log("Created stitcher.")

    def main_app_loop(self):
        """
        the actual meat of the application.

        UI -> Image input -> Image analysis -> Update UI -> repeat
        """
        self.__log("Started main app loop.")
        if self.gui:
            self.user_interface.mainloop()

    # the commandline main for the app
    def nogui_app_loop(self):
        instructions = """
        HELP DOC:

        p - make panorama from photos in path/
        s - save panorama to file
        a - analyse (broken - not possible)
        h - help; this usage message
        q - quit
        """
        prompt = "RoomScanner_2018> "
        while True:
            i = input(prompt)
            if i == 'p':
                path = input("Please enter a photo path: ")
                name = input("Please enter a name for this room: ")

                photoset = self.stitch_photo_set(name, path)
                print("Made pano named " + photoset.get_name())
            elif i == 's':
                # ask for name, ask for file, save file.
                print("INOMPLETE")
            elif i == 'a':
                print("analysis is not possible. Sorry! :(")
            elif i == 'h':
                print(instructions)
            elif i == 'q':
                self.quit_application()
            else:
                print("unknown command")

    #Take in from user_interface the path, convert into photoset, save panorama
    def stitch_photo_set(self, name, path):
        img_list = os.listdir(path)
        os.chdir(path)
        images = []

        for img in img_list:
            images.append(cv2.imread(img))

        new_photo_set = PhotoSet(name, images)

        temp = Stitcher.getInstance()
        #temp = Stitcher(name)
        panorama = temp.make_panorama(new_photo_set) #TODO save as file?
        print("Finished Stitching")

        self.photo_sets.append(new_photo_set)

		# panorama is a pair, panorama[1] would correspond to the stitched images
		# that gets stored in the Panorama object

        self.panoramas.append(Panorama(name, panorama[1]))

        return new_photo_set

    #Prompt user to show what the corners in the panorama are and then
    # gives diagram dimensions, assuming a rectangle
    #Takes in panorama, returns a a pair
    def analyze_panorama(self, panorama_name):
        panorama = self.get_panorama(panorama_name)
        width, length = Analyzer.getInstance().get_dimensions(panorama)

        #Swap width and length: assuming screen is wider than it is tall
        if width < length:
            temp = length
            length = width
            width = temp

        print("Analysis results: Width = ", width, "; Length = ", length)

        #Return to UI, to allow editting
        self.user_interface.showDiagram(width, length, panorama.get_name())


    def add_inventory(self, name):
        new_inventory = Inventory(name)
        self.inventories.append(new_inventory)
        return new_inventory

    def add_inventory_item(self, inventory_name, item_name, item_width, item_length):
        new_item = Item(item_name, item_width, item_length)
        inventory = self.get_inventory(inventory_name)
        inventory.add_item(new_item)
        print("Added item " + new_item.get_name() + "; "
              + str(new_item.get_width()) + ", " + str(new_item.get_length()))
        return new_item

    #Getter functions - by name
    def get_photo_set(self, name):
        for temp in self.photo_sets:
            if temp.get_name() == name:
                print(name, temp.get_name())
                return temp

        print("ERROR: Couldn't find photoset "+name+" in room_scanner.get_photo_set()")
        return None
    def get_panorama(self, name):
        for temp in self.panoramas:
            if temp.get_name() == name:
                return temp

        print("ERROR: Couldn't find panorama "+name+" in room_scanner.get_panorama()")
        return None
    def get_inventory(self, name):
        for temp in self.inventories:
            if temp.get_name() == name:
                return temp

        print("ERROR: Couldn't find inventory "+name+" in room_scanner.get_inventory()")
        return None
    def get_inventories(self):
        return self.inventories

    def get_inventory_size(self, name):
        for temp in self.inventories:
            if temp.get_name() == name:
                return temp.get_inventory_size()

        print("ERROR: Couldn't find inventory "+name+" in room_scanner.get_inventory_size()")
        return None

    def quit_application(self):
        """perform any necessary cleanup before quitting."""
        self.__log("Cleaning up.")
        exit(0)

    def __log(self, msg): # pylint: disable=no-self-use
        """private logging method, for later when logs aren't just prints"""
        print(msg)


def main(main_debug=False, main_gui=True):
    """by putting this in a function, we can test easier."""

    app = RoomScanner(debug=main_debug, gui=main_gui)
    if not main_gui:
        app.nogui_app_loop()
    else:
        app.main_app_loop()
        app.quit_application()


# Usage:
#   python3 room_scanner.py [-debug] [-nogui]

# pylint: disable=invalid-name
if __name__ == '__main__':
    try:
        sys.argv.index('-debug')  # look in args for debug
        d = True
        print('debugmode true')
    except ValueError:
        d = False

    try:
        sys.argv.index('-nogui')  # look in args for debug
        g = False
        print('nogui true')
    except ValueError:
        g = True

    main(main_debug=d, main_gui=g)
