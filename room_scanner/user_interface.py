# RoomScanner Main Application Script
#pylint: disable=invalid-name

# import sys
import os

import tkinter as tk #tkinter is for the GUI.
import tkinter.filedialog as fdialog
from tkinter import *
from PIL import Image, ImageTk

import cv2

#TODO rename to gui-related name
#Handle GUI
class user_interface():
    def __init__(self, room_scanner_parent, debug=False):
        self.root = Tk()

        #Debug mode?
        self.debug = debug

        self.root.title("RoomScanner")

        #self.top = Toplevel(self.root)
        #self.root.withdraw()
        #self.top.protocol("WM_DELETE_WINDOW", self.quit_application)
        #self.top.grid()

        self.createView()   #Custom, adds buttons

        self.photoset_buttons = []
        self.inventory_buttons = []
        self.room_scanner = room_scanner_parent


        if debug:
            print("Adding test case inventories")
            self.room_scanner.add_inventory("test inventory 1")
            self.room_scanner.add_inventory_item("test inventory 1", "item 1", 80, 50)
            self.room_scanner.add_inventory_item("test inventory 1", "item 2", 40, 60)
            self.room_scanner.add_inventory("test inventory 2")
            self.room_scanner.add_inventory_item("test inventory 2", "2item", 150, 100)


    #Fill frame with buttons/widgets/etc.
    #In the tkinter pdf, equivalent to createWidgets()
    def createView(self):
            #Create tabs
        self.tabs = tk.Frame(self.root)
        self.tabs.grid(row = 0)
        #self.tabs.pack(side=TOP)
        images_tab = tk.Button(
            self.tabs,
            text = "ImageBin",
            command = self.swap_tab_ImageBin
        )
        #images_tab.pack(side=LEFT)
        images_tab.grid(row=0, column=0)
        diagramsTab = tk.Button(
            self.tabs,
            text = "Diagrams",
            command = self.swap_tab_Diagrams
        )
        #diagramsTab.pack()
        diagramsTab.grid(row=0, column=1)
        inventoriesTab = tk.Button(
            self.tabs,
            text = "Room Inventories",
            command = self.swap_tab_Inventories
        )
        #inventoriesTab.pack(side=RIGHT)
        inventoriesTab.grid(row=0, column=2)

        self.root.rowconfigure(0, minsize=15)


            #Create main frames
        self.imagebin_frame = tk.Frame(self.root)
        self.imagebin_frame.grid(row = 1)
        #Showing initially, so don't remove, just do initial populating
        self.populate_imagebin_frame()
        self.curr_main_frame = self.imagebin_frame

        self.diagrams_frame = tk.Frame(self.root)
        self.diagrams_frame.grid(row = 1)
        self.diagrams_frame.grid_remove()
        #self.populateDiagramsFrame() nothing

        self.inventories_frame = tk.Frame(self.root)
        self.inventories_frame.grid(row = 1)
        self.inventories_frame.grid_remove()
        self.populateInventoriesFrame()

        #self.mainFrame = tk.Frame(self)
        #self.mainFrame.grid(row = 1)

        #self.quit_button.pack()

    #Initially populate the image bin frame
    def populate_imagebin_frame(self):
        self.upload_button = tk.Button(
            self.imagebin_frame,
            text='Open Your Images\' Folder',
            command=self.select_image_start
        )
        self.upload_button.grid(row=0, column=0)
        #self.upload_button.pack()

        self.quit_button = tk.Button(
            self.imagebin_frame,
            text='Quit',
            command=self.quit_application
        )
        self.quit_button.grid(row=0, column=1)

    #Initial populate inventories tab frame
    def populateInventoriesFrame(self):
        self.inventory_entry = Entry(
            self.inventories_frame,
            text = "New Inventory Name",
            bd = 5
        )
        self.inventory_entry.grid(row=0, column=0)

        self.new_inventory_button = tk.Button(
            self.inventories_frame,
            text='Add New Inventory',
            command=self.add_inventory
        )
        self.new_inventory_button.grid(row=0, column=1)

    #placeholders, will eventually swap/hide things
    def swap_tab_ImageBin(self):
        print("Swapped to ImageBin")
        self.curr_main_frame.grid_remove()    #Saves settings as well
        self.curr_main_frame = self.imagebin_frame
        self.curr_main_frame.grid(row = 1)
    def swap_tab_Diagrams(self):
        print("Swapped to Diagrams")
        self.curr_main_frame.grid_remove()
        self.curr_main_frame = self.diagrams_frame
        self.curr_main_frame.grid(row = 1)
    def swap_tab_Inventories(self):
        print("Swapped to Inventories")
        self.curr_main_frame.grid_remove()
        self.curr_main_frame = self.inventories_frame
        self.curr_main_frame.grid(row = 1)

    def select_image_start(self):
        guidelines_text="""Input for panorama should follow these guidelines:
-More overlap is better
-Name pictures 01, 02, … , 10, 11; not 1,2...10,11
-Things like TV screens changing will break the stitcher
-Files are either of type jpg or png"""

        top = Toplevel()
        msg=Message(top,text=guidelines_text,width=350)
        msg.pack()
        okay_button=Button(top,text='Okay',command = lambda :self.select_image(top))
        okay_button.pack()

    #upload_button's function
    def select_image(self, oldPopUp):
        oldPopUp.destroy()

        self.root.update()
        path = fdialog.askdirectory()   #Saves the string filepath
        self.root.update()

        self.text = path    #TODO not actually renaming at the moment?
        self.upload_button.config(state='disabled')

            #Send files for stitching
        photoset_name = os.path.basename(path)
        print("Received directory: " + path + ", name: " + photoset_name)
        if len(path) > 0:
            new_photo_set = self.roomScanner.stitch_photo_set(photoset_name, path)
        else:
            print("Error: No file path.")

        #Received panorama, show to user
        #pop-up w/ pano image, is this acceptable, yes/no?

        #no -> cancel? pass info by passing reference to pop-up?

            #Successfully added the new photoset
        #Add related button
        #TODO connect with room_scanner.py for saving photosets?
        newIndex = len(self.photoset_buttons)

        new_photoset_button = tk.Button(
            text = photoset_name,
            #new_photo_set, #photoset,
            #index = newIndex,
            master = self.imagebin_frame,
            command = lambda name=photoset_name: self.select_photo_set(name)
        )
        self.photoset_buttons.append(new_photoset_button)
        new_photoset_button.grid(row = 1+newIndex)

        #Add a corresponding button to diagrams tab
        new_diagram_button = tk.Button(
            text = photoset_name,
            master = self.diagrams_frame,
            command = lambda name = photoset_name: self.select_diagram(name)
        )
        self.photoset_buttons.append(new_diagram_button)
        new_diagram_button.grid(row = newIndex)

        #Reset
        self.upload_button.config(state = 'active')

    #Open the given diagram for analysis, and pass to user
    # Also includes instructions
    def select_diagram(self, name):
        #Display instructions popup
        instructions = "You should see the stitched panorama of your room.\
\nFirst, click the top two corners of one wall.\
\nThe display will then refresh. Click the top two corners of an adjacent wall.\
\n\nThe dimensions of the room will then be estimated.\
\nIf you would like to manually set the room dimensions, you will have the option to do so\
at the top right of the next window, when the resulting diagram is displayed."

        instrPopup = Toplevel()
        instrPopup.title("RoomScanner: Panorama Analysis Instructions")

        Message(  #instructions label
            instrPopup,
            text=instructions,
            justify = "left",
            width = 350
        ).grid(row = 0)

        #Button(instrPopup, text="Okay", command=instrPopup.destroy).grid(row = 1)

        #instrPopup.mainloop()
        instrPopup.update()

        self.room_scanner.analyze_panorama(name)


    #Start diagram making for given room
    def showDiagram(self, room_width, room_length, room_name):

        popup = Toplevel()
        popup.title("RoomScanner: Filling diagram "+ room_name)

        #Set width to show room to scale + space to list items
        #popup.configure(width = 10*room_width+55, height=10*room_length)

        #Left window/grid; add item canvases to this
        self.diagram = tk.Frame(
            popup,
            width = room_width,
            height = room_length,
            bg = "white")
        self.diagram.grid(column=0, row=0)
        self.diagram.pack_propagate(0)

        self.dimensions_label = Label(
            popup,
            text = "Dimensions: " + str(int(room_width)) + " by " + str(int(room_length))
        )
        self.dimensions_label.grid(column=0, row=1)


        #List inventorys to select on write -> will change to list of items in selected
        inventory_list = tk.Frame(popup, width=55)
        #inventory_list = tk.Listbox(popup, width=55)

        
        #Allow user to redefine room dimensions at top of inventory list
        new_width_entry = Entry(inventory_list, width=10, bd=2)
        new_width_entry.insert(0, "Room Width")
        new_width_entry.grid(row=0, column=0)

        new_length_entry = Entry(inventory_list, width=10, bd=2)
        new_length_entry.insert(0, "Room Length")
        new_length_entry.grid(row=0, column=1)

        Button(
            inventory_list,
            text = "Set New Dimensions",
            command = lambda:
                self.set_diagram_dimensions(new_width_entry, new_length_entry, self.diagram)
        ).grid(row=0, column=2)

        #Fill with right listbox inventories from room_scanner
            #use .destroy() later to get rid of
        currIndex = 1
        for inventory in self.room_scanner.get_inventories():
            name = inventory.get_name()
            Button(
                inventory_list,
                text = name,
                command = lambda i=inventory:
                    self.select_diagram_inventory(i, inventory_list, popup)
            ).grid(row = currIndex)
            currIndex += 1

        inventory_list.grid(column=1, row=0, sticky="N")

        popup.mainloop()

    #Allow set new diagram dimensions, used in popup
    def set_diagram_dimensions(self, width_entry, length_entry, diagram):
        newWidth = int(width_entry.get()) #*10
        newLength = int(length_entry.get()) #*10
        diagram.configure(
            width = newWidth,
            height = newLength
        )
        self.dimensions_label.config(
            text = "Dimensions: " + str(newWidth) + " by " + str(newLength))


    #Button command for selecting inventory to use in a diagram
    def select_diagram_inventory(self, inventory, inventory_list, popup):
        #Replace inventory_list with itemsList
        inventory_list.destroy()
        itemsList = tk.Frame(popup, width=55)

        #Save button - feature potentially removed, may require significant refactoring
        '''Button(
            itemsList,
            text = "Save Diagram as Image",
            command = self.saveDiagram
        ).grid(row = 0)'''

        currIndex = 1 #Track current row, keep consistent and increment in parallel to item
        for item in inventory.get_items():
            #Each item gets its own row

            width = item.get_width()
            length = item.get_length()

            Button( #Button to add the given item to the diagram
                itemsList,
                text = item.get_name(),
                command = lambda i=item: self.add_item_to_diagram(i)
            ).grid(row=currIndex, column=0, sticky="N")

            #Add a label for the item's dimensions:
            width_label = Label(
                itemsList,
                text = str(width) + " by "
            )
            width_label.grid(row=currIndex, column=1, sticky="N")
            length_label = Label(
                itemsList,
                text = length
            )
            length_label.grid(row=currIndex, column=2, sticky="N")

            #Button to rotate this inventory item
            Button(
                itemsList,
                text = "Rotate",
                command = lambda i=item,
                    w = width_label,
                    l = length_label:
                    self.diagramItemRotate(i, w, l)
            ).grid(row=currIndex, column=3, sticky="N")

            currIndex += 1


        itemsList.grid(column=1, row=0, sticky="N")

    #Diagram function's rotate item button and update labels on diagram menu
    def diagramItemRotate(self, item, width_label, length_label):
        item.rotate()
        width_label.configure(text = str(item.get_width()) + " by ")
        length_label.configure(text = item.get_length())

    # def saveDiagram(self):
    #     x = self.diagram.winfo_rootx()+self.widget.winfo_x()
    #     y = self.diagram.winfo_rooty()+self.widget.winfo_y()
    #
    #     x1 = x + self.widget.winfo_width()
    #     y1 = y + self.widget.winfo_height()

        #pyscreenshot.grab(bbox = (x, y, x1, y1)).save("diagram")


    #For use/button when diagram open and inventory selected
    def add_item_to_diagram(self, item):
        ItemIcon(origin_item=item, master=self.diagram).pack()
        #Add item, will be able to drag and move around


    #Given photoset at index, access the photoset
    def select_photo_set(self, name):
        currPhotoset = self.room_scanner.get_photo_set(name)

        print("Selected photoset " + currPhotoset.get_name())

        #Get and display the panorama in a pop-up
        pano = self.room_scanner.get_panorama(currPhotoset.get_name())

        popup = Tk()
        popup.title("RoomScanner: " + pano.get_name()+" Panorama")


        # grab the panorama
        cv_im = pano.get_pano()

        #resize the image to get to fit better onto the screen
        cv_im = cv2.resize(cv_im, (int(cv_im.shape[1]*.4), int(cv_im.shape[0]*.4)))

        #convert cv2 image to Image
        image = Image.fromarray(cv2.cvtColor(cv_im, cv2.COLOR_BGR2RGB))

        #convert Image to ImageTk
        im = ImageTk.PhotoImage(image, master=popup)


        #add the ImageTk object to the pop window
        label = Label(popup, image=im)
        label.image = im
        display = im
        label.pack(side=tk.TOP, fill=tk.BOTH, expand=tk.YES)


        '''
        img = Image.fromarray(pano.get_pano())
        imgTk = ImageTk.PhotoImage(image = img)

        Label(root=popup, image=imgTk).pack()'''

        popup.mainloop() #Have user close manually


    #Add a new initially empty inventory: create popup to get name
    def add_inventory(self):
        inventory = self.room_scanner.add_inventory(self.inventory_entry.get())

        #Add corresponding button
        len_inventory_buttons = len(self.inventory_buttons)
        new_inventory_button = tk.Button(
            text = inventory.get_name(),
            #index = len_inventory_buttons,
            master = self.inventories_frame,
            command = lambda: self.open_add_item_menu(inventory))

        self.inventory_buttons.append(new_inventory_button)
        new_inventory_button.grid()

    def open_add_item_menu(self, inventory):
        popup = Toplevel()
        popup.title("RoomScanner: Adding Inventory Items to "+ inventory.get_name())

        inventory_label = Label(popup, text="Adding to " + inventory.get_name()
            + ".\n " + str(inventory.get_inventory_size())+" items currently.")
        inventory_label.grid(row = 0)

        name_label = Label(popup, text="New Item Name")
        name_label.grid(row=1, column=0)
        name_entry = Entry(popup, bd=3)
        name_entry.grid(row=1, column=1)

        width_label = Label(popup, text="New Item Width")
        width_label.grid(row=2, column=0)
        width_entry = Entry(popup, bd=3)
        width_entry.grid(row=2, column=1)

        length_label = Label(popup, text="New Item Length")
        length_label.grid(row=3, column=0)
        length_entry = Entry(popup, bd=3)
        length_entry.grid(row=3, column=1)

        #TODO check for string/numbers

        #Button to confirm add item
        confirm_button = Button(
            popup,
            text = "Confirm",
            command = lambda: self.add_item(
                inventory.get_name(),
                name_entry,
                width_entry,
                length_entry,
                list_string)
        )
        confirm_button.grid(row=4, column=0)

        #Quit Button
        Button(
            popup,
            text = "Save and Close",
            command = popup.destroy
        ).grid(row=4, column=1)

        #Create and fill a list label
        list_string = StringVar()
        list_label = Label(popup, textvariable=list_string, justify=LEFT)

        #Ensure list_label is updated
        temp_string = "Items:\nName \t\t Length (ft) \t Width (ft)"
        for item in inventory.get_items():
            #Keep list_string updated
            temp_string += ("\n"+item.get_name()
                            + "\t\t"+str(item.get_width())
                            + "\t\t"+str(item.get_length()))
            #TODO Note: display incorrect w/ multiple windows open, concurrent changes
        list_string.set(temp_string)
        list_label.grid(row = 5)

        #TODO add save(?) and quit

        popup.mainloop()


    #Parameters: name of inventory adding to, pointers to tk.entry,
    #   and listing label's string
    #Adds the item of specified values to an inventory of specified name
    def add_item(self, inventory_name, name_entry, width_entry, length_entry, list_string):
        #Collect values from entries
        name = name_entry.get()
        width = int(width_entry.get())
        length = int(length_entry.get())

        self.room_scanner.add_inventory_item(inventory_name, name, width, length)

        #Update the popup's list of items
        list_string.set(list_string.get() +
                        "\n"+name+"\t\t"+str(width)+"\t\t"+str(length))


    def mainloop(self):
        self.root.mainloop()

    #Delegate most of stuff to room_scanner
    def quit_application(self):
        # do any cleanup
        self.root.destroy()
        self.room_scanner.quit_application()

    #TODO error taking
    #def errorPopUp(self):
    # Take in an arbitrary list, first few indices defined
    # [Error string, type, information needed (photoset indices/names, etc.)]


#Extend a tk.button, just hold photoset reference as well
# Automate some of the photoset-related handling
# Should be held in user_interface
class PhotoSetButton(tk.Button):
    def __init__(self, photoset, index, *args, **kwargs):
        tk.Button.__init__(self, *args, **kwargs)

        self['text'] = photoset.get_name()

        self.photoset = photoset    #Keep reference to the related photoset
        self.grid(row = 2 + index)

class InventoryButton(tk.Button): #TODO remove, was able to avoid using
    def __init__(self, inventory, index, *args, **kwargs):
        tk.Button.__init__(self, *args, **kwargs)

        self['text'] = inventory.get_name() #TODO list/update size or something?

        self.inventory = inventory    #Keep reference to the related inventory
        self.grid(row = 2 + index)

class ItemIcon(tk.Canvas):
    def __init__(self, origin_item, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.drag_start_x = 0
        self.drag_start_y = 0
        self.bind("<Button-1>", self.drag_start)
        self.bind("<B1-Motion>", self.drag_motion)

        #Adjust to correct setting according to provided inventory item
        self.item = origin_item
        self.configure(
            width = self.item.get_width(),
            height = self.item.get_length(),
            background = "white",
            borderwidth = 3,
            relief = "raised")

        #Internal item label
        self.create_text(
            self.item.get_width() /2, #TODO may be able to use text length to center
            self.item.get_length() /2+8,
            justify = "center",
            anchor = "s",
            text = self.item.get_name()
        )


    def drag_start(self, event):
        self.drag_start_x = event.x
        self.drag_start_y = event.y

    def drag_motion(self, event):
        x = self.winfo_x() - self.drag_start_x + event.x
        y = self.winfo_y() - self.drag_start_y + event.y
        self.place(x=x, y=y) #TODO doesn't account for bounding
