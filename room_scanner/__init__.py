"""The room_scanner module contains all submodules
needed for the RoomScanner app. See room_scanner.py
for detail.
"""

__all__ = [
    'stitcher',
    'photo_set',
    'analyzer',
    'user_interface'
]

from room_scanner import main
