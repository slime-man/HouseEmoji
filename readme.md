README.

Ok thanks.

## Versioning
  * Python 3.5.2
  * numpy (1.14.0) - BSD-new License - http://www.numpy.org/license.html
  * opencv-python (3.4.0.12) - BSD-License - https://opencv.org/license.html
  * Tkinter (8.6) - GPL License
  * Pillow-5.0.0 - https://raw.githubusercontent.com/python-pillow/Pillow/master/LICENSE

Stitcher-class object required opencv version 3.3 or higher
in order to work
